# https://docs.python.org/3/library/pickle.html
# Pickling is fine when reading/writing your own files but NOT to be used with untrusted files
import pickle

imelda = (
    'More Mayhem',
    'IMelda May',
    '2011',
    (
        (1, 'Pulling the Rug'),
        (2, 'Psycho'),
        (3, 'Mayhem'),
        (4, 'Kentish Town Waltz')
    )
)

# Takes care of our complex object and makes it easier when loading later
with open('imelda.pickle', 'wb') as pickle_file:
    pickle.dump(imelda, pickle_file)

# reads the data and stores it in the new object
with open('imelda.pickle', 'rb') as imelda_pickled:
    imelda2 = pickle.load(imelda_pickled)

print(imelda2)
album, artist, year, track_list = imelda2
print(album)
print(artist)
print(year)
for track in track_list:
    track_number, track_title = track
    print(track_number, track_title)

print('=' * 40)

# All different kinds of object types

imelda = (
    'More Mayhem',
    'IMelda May',
    '2011',
    (
        (1, 'Pulling the Rug'),
        (2, 'Psycho'),
        (3, 'Mayhem'),
        (4, 'Kentish Town Waltz')
    )
)

even = list(range(0, 10, 2))
odd = list(range(1, 10, 2))
with open('imelda2.pickle', 'wb') as pickle_file:
    pickle.dump(imelda, pickle_file)
    pickle.dump(even, pickle_file)
    pickle.dump(odd, pickle_file)
    pickle.dump(2998302, pickle_file)

# NOTE: Objects MUST be read back in the same order
with open('imelda2.pickle', 'rb') as imelda_pickled:
    imelda2 = pickle.load(imelda_pickled)
    even_list = pickle.load(imelda_pickled)
    odd_list = pickle.load(imelda_pickled)
    x = pickle.load(imelda_pickled)

print(imelda2)
album, artist, year, track_list = imelda2
print(album)
print(artist)
print(year)
for track in track_list:
    track_number, track_title = track
    print(track_number, track_title)

print('=' * 40)
for i in even_list:
    print(i)

print('=' * 40)

for i in odd_list:
    print(i)

print('=' * 40)

print(x)

# ----------------------------------------------------------------------------------------------------
# Protocols
# NOTE: If you used a different pickle protocol in a different version you CAN NOT re load the same data again
# protocol = 3 is the python version 3 and is used by default
# Data created using protocol 3 will not be able to be read by python 2
# ----------------------------------------------------------------------------------------------------
print('=' * 40)

imelda = (
    'More Mayhem',
    'IMelda May',
    '2011',
    (
        (1, 'Pulling the Rug'),
        (2, 'Psycho'),
        (3, 'Mayhem'),
        (4, 'Kentish Town Waltz')
    )
)

even = list(range(0, 10, 2))
odd = list(range(1, 10, 2))
with open('imelda3.pickle', 'wb') as pickle_file:
    pickle.dump(imelda, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)
    # protocol=0 Much easier to read
    pickle.dump(even, pickle_file, protocol=0)
    pickle.dump(odd, pickle_file, protocol=pickle.DEFAULT_PROTOCOL)
    pickle.dump(2998302, pickle_file, protocol=pickle.DEFAULT_PROTOCOL)

# python will know which protocol
with open('imelda2.pickle', 'rb') as imelda_pickled:
    imelda2 = pickle.load(imelda_pickled)
    even_list = pickle.load(imelda_pickled)
    odd_list = pickle.load(imelda_pickled)
    x = pickle.load(imelda_pickled)

print(imelda2)
album, artist, year, track_list = imelda2
print(album)
print(artist)
print(year)
for track in track_list:
    track_number, track_title = track
    print(track_number, track_title)

print('=' * 40)
for i in even_list:
    print(i)

print('=' * 40)

for i in odd_list:
    print(i)

print('=' * 40)

print(x)
