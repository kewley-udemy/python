# simple as specifying 'b' for binary
# Note: That a string/int can't go directly to bytes, have to use a built-in function 'bytes'
with open('binary', 'bw') as binary_file:
    for i in range(17):
        # enclosing within an array otherwise it won't write it correctly
        # byte works a little strange, if you pass an int to it it creates that many bytes in a list, e.g.
        # 17 would produce \x00 * 17 times.
        binary_file.write(bytes([i]))
    # Better way
    # binary_file.write(bytes(range(17)))

with open('binary', 'br') as binary_file:
    for b in binary_file:
        print(b)

a = 65534  # FF FE
b = 65535  # FF FF
c = 65536  # 00 01 00 00
x = 2998302  # 00 2D C0 1E

with open("binary2", 'bw') as bin_file:
    # how many bytes to return, and whether or not we want big-endian or little-endian
    # Most significant byte first remaining bytes following in order, same way you would write it
    bin_file.write(a.to_bytes(2, 'big'))
    bin_file.write(b.to_bytes(2, 'big'))
    # need at least 3 bytes to represent this number, but we want an even number so we go up to 4
    bin_file.write(c.to_bytes(4, 'big'))
    bin_file.write(x.to_bytes(4, 'big'))
    # reverse of the big endian
    bin_file.write(x.to_bytes(4, 'little'))

with open("binary2", 'br') as bin_file:
    e = int.from_bytes(bin_file.read(2), 'big')
    print(e)
    f = int.from_bytes(bin_file.read(2), 'big')
    print(f)
    g = int.from_bytes(bin_file.read(4), 'big')
    print(g)
    h = int.from_bytes(bin_file.read(4), 'big')
    print(h)
    i = int.from_bytes(bin_file.read(4),
                       'big')  # gives unexpected result since we saved it as little and read it as big
    print(i)
