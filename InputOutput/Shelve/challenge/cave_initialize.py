import shelve

# ---------------------------------------------------------------------------------------------------------------------
# Solution 1 naive
# ---------------------------------------------------------------------------------------------------------------------

# Great, but disadvantage may be that as you add larger data sets this can cause complications
# There are advantages and disadvantages to most approaches, and you had to make fewer changes to the
# code - a definite advantage.
#
# The disadvantage of your approach is that you've effectively read the entire dictionaries into memory.
# If they are very large, you lose the advantage of using a shelve - that the data exists on disk and is only
# loaded into memory as you access each value.
#
# With dictionaries the size of these, that's not a problem; but it's something to bear in mind when
# dealing with very large sets of data.
with shelve.open("cave_data") as cave_data:
    cave_data["locations"] = {0: {"desc": "You are sitting in front of a computer learning Python",
                                  "exits": {},
                                  "namedExits": {}},
                              1: {"desc": "You are standing at the end of a road before a small brick building",
                                  "exits": {"W": 2, "E": 3, "N": 5, "S": 4, "Q": 0},
                                  "namedExits": {"2": 2, "3": 3, "5": 5, "4": 4}},
                              2: {"desc": "You are at the top of a hill",
                                  "exits": {"N": 5, "Q": 0},
                                  "namedExits": {"5": 5}},
                              3: {"desc": "You are inside a building, a well house for a small stream",
                                  "exits": {"W": 1, "Q": 0},
                                  "namedExits": {"1": 1}},
                              4: {"desc": "You are in a valley beside a stream",
                                  "exits": {"N": 1, "W": 2, "Q": 0},
                                  "namedExits": {"1": 1, "2": 2}},
                              5: {"desc": "You are in the forest",
                                  "exits": {"W": 2, "S": 1, "Q": 0},
                                  "namedExits": {"2": 2, "1": 1}}
                              }
    cave_data["vocabulary"] = {"QUIT": "Q",
                               "NORTH": "N",
                               "SOUTH": "S",
                               "EAST": "E",
                               "WEST": "W",
                               "ROAD": "1",
                               "HILL": "2",
                               "BUILDING": "3",
                               "VALLEY": "4",
                               "FOREST": "5"}

# ---------------------------------------------------------------------------------------------------------------------
# Solution 2 better, less memory
# ---------------------------------------------------------------------------------------------------------------------
with shelve.open("locations") as locations:
    locations['0'] = {"desc": "You are sitting in front of a computer learning Python",
                      "exits": {},
                      "namedExits": {}}
    locations['1'] = {"desc": "You are standing at the end of a road before a small brick building",
                      "exits": {"W": "2", "E": "3", "N": "5", "S": "4", "Q": "0"},
                      "namedExits": {"2": "2", "3": "3", "5": "5", "4": "4"}}
    locations['2'] = {"desc": "You are at the top of a hill",
                      "exits": {"N": "5", "Q": "0"},
                      "namedExits": {"5": "5"}}
    locations['3'] = {"desc": "You are inside a building, a well house for a small stream",
                      "exits": {"W": "1", "Q": "0"},
                      "namedExits": {"1": "1"}}
    locations['4'] = {"desc": "You are in a valley beside a stream",
                      "exits": {"N": "1", "W": "2", "Q": "0"},
                      "namedExits": {"1": "1", "2": "2"}}
    locations['5'] = {"desc": "You are in the forest",
                      "exits": {"W": "2", "S": "1", "Q": "0"},
                      "namedExits": {"2": "2", "1": "1"}}

with shelve.open('vocabulary') as vocabulary:
    vocabulary["QUIT"] = "Q"
    vocabulary["NORTH"] = "N"
    vocabulary["SOUTH"] = "S"
    vocabulary["EAST"] = "E"
    vocabulary["WEST"] = "W"
    vocabulary["ROAD"] = "1"
    vocabulary["HILL"] = "2"
    vocabulary["BUILDING"] = "3"
    vocabulary["VALLEY"] = "4"
    vocabulary["FOREST"] = "5"
