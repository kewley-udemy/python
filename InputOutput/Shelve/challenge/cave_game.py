import shelve

# Solution 1 using naive approach
# with shelve.open('cave_data') as cave_data:
#     loc = 1
#     while True:
#         locations = cave_data["locations"]
#         vocabulary = cave_data["vocabulary"]
#         availableExits = ", ".join(locations[loc]["exits"].keys())
#
#         print(locations[loc]["desc"])
#
#         if loc == 0:
#             break
#         else:
#             allExits = locations[loc]["exits"].copy()
#             allExits.update(locations[loc]["namedExits"])
#
#         direction = input("Available exits are " + availableExits + " ").upper()
#         print()
#
#         # Parse the user input, using our vocabulary dictionary if necessary
#         if len(direction) > 1:  # more than 1 letter, so check vocab
#             words = direction.split()
#             for word in words:
#                 if word in vocabulary:  # does it contain a word we know?
#                     direction = vocabulary[word]
#                     break
#
#         if direction in allExits:
#             loc = allExits[direction]
#         else:
#             print("You cannot go in that direction")

# Solution 2
with shelve.open('locations') as locations, shelve.open('vocabulary') as vocabulary:
    loc = "1"
    while True:
        availableExits = ", ".join(locations[loc]["exits"].keys())

        print(locations[loc]["desc"])

        if loc == "0":
            break
        else:
            allExits = locations[loc]["exits"].copy()
            allExits.update(locations[loc]["namedExits"])

        direction = input("Available exits are " + availableExits + " ").upper()
        print()

        # Parse the user input, using our vocabulary dictionary if necessary
        if len(direction) > 1:  # more than 1 letter, so check vocab
            words = direction.split()
            for word in words:
                if word in vocabulary:  # does it contain a word we know?
                    direction = vocabulary[word]
                    break

        if direction in allExits:
            loc = allExits[direction]
        else:
            print("You cannot go in that direction")
