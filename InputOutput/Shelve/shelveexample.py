# Think of it like a dictionary but stored in a file rather than in memory
# Keys must be strings though
# All dictionary methods can be applied to shelves
# Like pickle, do not use shelve files from untrusted sources
import shelve

with shelve.open('ShelfTest') as fruit:
    fruit['orange'] = "a sweet, orange, citrus fruit"
    fruit['apple'] = "good for making cider"
    fruit['lemon'] = "a sour, yellow citrus fruit"
    fruit['grape'] = "a small, sweet fruit growing in bunches"
    fruit['lime'] = "a sour, green citrus fruit"

    # This won't work, it will instead store a dictionary rather than a Shelve object
    # fruit = {
    #     "orange": "a sweet, orange, citrus fruit",
    #     "apple": "good for making cider",
    #     "lemon": "a sour, yellow citrus fruit",
    #     "grape": "a small, sweet fruit growing in bunches",
    #     "lime": "a sour, green citrus fruit"
    # }
    print(fruit['lemon'])
    print(fruit['grape'])
    print(fruit)



