import shelve

blt = ["bacon", "lettuce", "tomato", "bread"]
beans_on_toast = ["beans", "bread"]
scrambled_eggs = ["eggs", "butter", "milk"]
soup = ["tin of soup"]
pasta = ["pasta", "cheese"]

with shelve.open("recipes") as recipes:
    recipes["blt"] = blt
    recipes["beans on toast"] = beans_on_toast
    recipes["scrambled eggs"] = scrambled_eggs
    recipes["pasta"] = pasta

with shelve.open("recipes") as recipes:
    recipes["soup"] = soup

    for snack in recipes:
        print(snack, recipes[snack])

print("=" * 40)

with shelve.open("recipes") as recipes:
    # This just appends to a COPY, this does NOT modify the underlying shelve file
    recipes["blt"].append("butter")
    recipes["pasta"].append("tomato")

    for snack in recipes:
        print(snack, recipes[snack])

print("=" * 40)

# One way to modify the objects is to make a temp copy and update the copy then update the data.
with shelve.open("recipes") as recipes:
    temp_list = recipes["blt"]
    temp_list.append("butter")
    recipes["blt"] = temp_list

    temp_list = recipes["pasta"]
    temp_list.append("tomato")
    recipes["pasta"] = temp_list

    for snack in recipes:
        print(snack, recipes[snack])

print("=" * 40)

# Better to not use the below method here:
# Other way to modify the objects is to add a write back with the shelve
# NOTE: This will write to a cache and will not write back  to the file until you close it or call 'sync()'
# Disadvantage is possible Heavy memory usage
with shelve.open("recipes", writeback=True) as recipes:
    recipes["soup"].append("croutons")

    for snack in recipes:
        print(snack, recipes[snack])

with shelve.open("recipes", writeback=True) as recipes:
    recipes["soup"] = soup
    recipes.sync()  # clears the cache
    soup.append("cream")  # this will be ignored now

    for snack in recipes:
        print(snack, recipes[snack])
