import shelve

with shelve.open('ShelfMod') as fruit:
    fruit['orange'] = "a sweet, orange, citrus fruit"
    fruit['apple'] = "good for making cider"
    fruit['lemon'] = "a sour, yellow citrus fruit"
    fruit['grape'] = "a small, sweet fruit growing in bunches"
    fruit['lime'] = "a sour, green citrus fruit"

    print(fruit["lime"])
    fruit["lime"] = "great with tequila"
    print(fruit["lime"])

    while True:
        shelf_key = input("Please enter a fruit: ")
        if shelf_key == "quit":
            break

        # Can use get method to handle invalid keys safely
        description = fruit.get(shelf_key, "We don't have a " + shelf_key)
        # Can also use in
        # if shelf_key in fruit:
        print(description)

    print("=" * 40)
    # sorting
    ordered_keys = list(fruit.keys())
    ordered_keys.sort()
    for f in ordered_keys:
        print(f + " - " + fruit[f])

    print("=" * 40)

    # Instead of Dict Items they return ValuesView
    for v in fruit.values():
        print(v)
    print(fruit.values())
    for f in fruit.items():
        print(f)
    print(fruit.items())
