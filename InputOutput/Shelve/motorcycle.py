import shelve

with shelve.open("bike") as bike:
    bike["make"] = "Honda"
    bike["model"] = "250 dream"
    bike["color"] = "red"
    bike["engine_size"] = 250

    # removes the key from the file
    # del bike["engine_size"]

    for key in bike:
        print(key)
    print('=' * 40)

    print(bike["engine_size"])
    print(bike["color"])

with shelve.open("bike2") as bike:
    bike["make"] = "Honda"
    bike["model"] = "250 dream"
    bike["color"] = "red"
    # fat finger mistake
    bike["engin_size"] = 250

    # error arises here
    # print(bike["engine_size"])
    print(bike["color"])


# Issue is if you try to fix the above by adding engine_size, you will now have BOTH keys in the underlying database
# You will have engin_size and engine_size, so just correcting the code doesn't fix the underlying file.
