import shelve

books = shelve.open("book")
books["recipes"] = {
    "blt": ["bacon", "lettuce", "tomato", "bread"],
    "beans_on_toast": ["beans", "bread"],
    "scrambled_eggs": ["eggs", "butter", "milk"],
    "soup": ["tin of soup"],
    "pasta": ["pasta", "cheese"]
# },  THIS CAUSES ERRORS, SO BE CAREFUL WHEN CONVERTING OLD DICTIONARIES TO SHELVES, see that comma there
}
books["maintenance"] = {
    "stuck": ["oil"],
    "loose": ["gaffer tape"]
}

print(books["recipes"]["soup"])
print(books["recipes"]["scrambled_eggs"])

print(books["maintenance"]["loose"])

books.close()
