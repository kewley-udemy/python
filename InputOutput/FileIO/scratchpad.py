# Removes character from beginnine or end and ONLY the beginning or end
print("Adelaide".strip("A"))
# Won't work but will remove the 'de' at the end since it just checks for "partial" matches
print("Adelaide".strip("del"))
