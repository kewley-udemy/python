cities = ["Adelaide", "Alice Springs", "Darwin", "Melbourne", "Sydney"]

with open("cities.txt", 'w') as city_file:
    for city in cities:
        # Computer places each text into a buffer and the contents of the buffer are written to the file
        # Sometimes you want it to be written right away
        # Generally speaking, you won't need to use this since modern day computers are very fast
        print(city, file=city_file, flush=True)

read_cities = []
with open("cities.txt", 'r') as city_file:
    for city in city_file:
        read_cities.append(city.strip('\n\r'))

print(read_cities)
for city in read_cities:
    print(city)


imelda = "More Mayhem", "Imelda May", "2011", (
    (1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")
)
# writes the fuple into a string value, but hard to read that back to a tuple
with open("imelda3.txt", "w") as imelda_file:
    print(imelda, file=imelda_file)

with open("imelda3.txt", 'r') as imelda_file:
    contents = imelda_file.readline()

# Not an ideal way to read back a Tuple, we will learn a better way in the near future.
# The reason is file content can change formatting
imelda = eval(contents)

print(imelda)
title, artist, year, tracks = imelda
print(title)
print(artist)
print(year)
print(tracks)
