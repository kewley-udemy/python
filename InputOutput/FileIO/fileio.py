# r - stands for read mode
# https://docs.python.org/3/library/functions.html#open

jabber = open("sample.txt", "r")

for line in jabber:
    if "jabberwock" in line.lower():
        print(line, end='')

# File can become corrupted if say you kept it open and were writing to it
jabber.close()

print('='*40)

# ----------------------------------------------------------------------------------------------------
# File API
# readLine -> reads a single line and returns a string (better for not overloading memories)
# readLines -> reads all the lines in the file and returns an array of strings (problems since it reads it all)
# read -> reads the whole file, and if it's a string it stores it as a string (can be useful for bytes)
# ----------------------------------------------------------------------------------------------------

# Using "with" which handles closing things automatically
with open("sample.txt", 'r') as jabber:
    for line in jabber:
        if "JAB" in line.upper():
            print(line, end='')


print('='*40)

# One by one reading lines
with open("sample.txt", "r") as jabber:
    line = jabber.readline()
    while line:
        print(line, end='')
        line = jabber.readline()

print('='*40)

# Read the whole file at once
with open("sample.txt", "r") as jabber:
    lines = jabber.readlines()

# Uses a list, notice how there is a '\n' character in each row, which is why we are doing the end='' above
print(lines)


# # Can do it in reverse for fun
# with open("sample.txt", "r") as jabber:
#     lines = jabber.readlines()
#
# for line in lines[::-1]:
#     print(line, end='')

