fruit = {"orange": "a sweet, orange, citrus fruit",
         "apple": "good for making cider",
         "lemon": "a sour, yellow citrus fruit",
         "grape": "a small, sweet fruit growing in bunches",
         "lime": "a sour, green citrus fruit"
         }

print(fruit)

# while True:
#     dict_key = input("Please enter a fruit")
#     if dict_key == "quit":
#         break
#     # using default value instead
#     description = fruit.get(dict_key, "We don't have a '{}'".format(dict_key))
#     print(description)

# What way do we choose? Depends on functionality if you need to do something a bit more
# then use the in method, there is also the has_key which is deprecated in python 3
# fruit.has_key(dict_key) is equivalent to dict_key in fruit

# iterating over keys
for snack in fruit:
    print(fruit[snack])
print("-"*40)

"""
In Python 3.,6 and later, the ordering of keys in a dictionary is preserved.  That means they'll 
always print in the same order.

However, the documentation does state that you shouldn't rely on this behaviour.  So the point about explicitly
 sorting the keys still holds - if your code relies on iterating over the keys in a certain order, 
 then sort them first.
"""
# Python < 3.6 this will print out different orders on each RUN not on each iteration since it uses
# a hash function and has no guarantee on order
# for i in range(10):
#     for snack in fruit:
#         print(snack + " is " + fruit[snack])
#     print('-'*40)

# View objects
print(fruit.keys())
print(fruit.values())
print("-"*40)

fruit_keys = fruit.keys()
print(fruit_keys)
fruit["tomato"] = "not nice with ice cream"
# notice how this is automatically update!
print(fruit_keys)
print("-"*40)

# Sorting
for f in sorted(list(fruit.keys())):
    print(f + " - " + fruit[f])
print("-"*40)

# Less efficient
for val in fruit.values():
    print(val)
print("-"*40)

# More efficient
for key in fruit:
    print(fruit[key])
print("-"*40)
