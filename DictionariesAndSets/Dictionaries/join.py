myList = ["a", "b", "c", "d"]
newString = ''
# inefficient because every time we loop through we create a new copy since string is immutable
for c in myList:
    newString += c + ", "
print(newString)
print("-"*40)

# Using join instead
diffString = ", ".join(myList)
print(diffString)

