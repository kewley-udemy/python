locations = {
    0: "You are sitting in front of a computer learning python",
    1: "You are standing at the end of a road before a small brick building",
    2: "You are at the top of a hill",
    3: "You are inside a building, a well house for a small stream",
    4: "You are in a valley beside a stream",
    5: "You are in the forest"
}

# Each dictionary is key as to where we can go, the value is the key for the locations object above
# Each list item represents the DIRECTIONS you can go in the locations above
# Using list of dictionaries because a LIST is has indexes
# So, index 0 represents the "quit" case and we are back at our computer
# Index 1 is represents the key 1 from above in locations
exits = [
    {"Q": 0},  # computer
    {"W": 2, "E": 3, "N": 5, "S": 4, "Q": 0},  # end of a road and different directions we can go
    {"N": 5, "Q": 0},  # top of a hil;
    {"W": 1, "Q": 0},  # inside a building
    {"N": 1, "W": 2, "Q": 0},  # in a valley
    {"W": 2, "S": 1, "Q": 0}  # forest
]

loc = 1
while True:
    availableExits = ", ".join(exits[loc].keys())
    print(locations[loc])

    if loc == 0:
        break

    direction = input("Available exits are " + availableExits + " ").upper()
    print()
    if direction in exits[loc]:
        loc = exits[loc][direction]
    else:
        print("You cannot go in that direction")

