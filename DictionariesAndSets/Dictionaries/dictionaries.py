# Accessed by means of a key rather than an index
fruit = {"orange": "a sweet, orange, citrus fruit",
         "apple": "good for making cider",
         "lemon": "a sour, yellow citrus fruit",
         "grape": "a small, sweet fruit growing in bunches",
         "lime": "a sour, green citrus fruit",
         # the dictionary takes the LAST key it sees, notice there are two but this is used
         "apple": "round and crunchy"}

print(fruit)
# key access, notice how IntelliJ is smart enough to know what keys are available
print(fruit["lemon"])

# Keys can be an IMMUTABLE object, so we can't for example have a list but we can have a tuple
fruit[(1, 2)] = "What"
print(fruit[(1, 2)])

# Adding
fruit["pear"] = "an odd shaped apple"
print(fruit)

# Updating
fruit["pear"] = "great with tequila"
print(fruit)

# Removing
del fruit["lemon"]
print(fruit)

# WARNING: If you don't specify a key it deletes the WHOLE variable completely

# Clearing the dictionary
# fruit.clear()
# print(fruit)

# Will give a KeyError, so how do we test if something is not in a dictionary??
# print(fruit["tomato"])

while True:
    dict_key = input("Please enter a fruit")
    if dict_key == "quit":
        break
    # how we can verify a dictionary contains a particular key
    if dict_key in fruit:
        # the get DOES NOT give an error if the key does not exist
        description = fruit.get(dict_key)
        print(description)
    else:
        print("We don't have {}".format(dict_key))

