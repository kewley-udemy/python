fruit = {"orange": "a sweet, orange, citrus fruit",
         "apple": "good for making cider",
         "lemon": "a sour, yellow citrus fruit",
         "grape": "a small, sweet fruit growing in bunches",
         "lime": "a sour, green citrus fruit"
         }

print(fruit)
print("-"*40)

# Dynamic view object that consists of key and value tuples
print(fruit.items())
print("-"*40)
f_tuple = tuple(fruit.items())
print(f_tuple)
print("-"*40)
for snack in f_tuple:
    item, description = snack
    print(item + " is " + description)
print("-"*40)
# Going the other way from tuple to dict
print(dict(f_tuple))


