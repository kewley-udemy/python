# Unordered and DOES not contain duplicates
# Unlike a dictionary, they are not accessed by key
# Objects must be immutable
# https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset

farmAnimals = {"sheep", "cow", "hen"}
print(farmAnimals)

for animal in farmAnimals:
    print(animal)

print("=" * 40)

# NOTE to create an empty set you HAVE to use set(), if you just did {} it would create a dictionary
emptySet = set()  # set object
emptySetTwo = {}  # dictionary object

print("=" * 40)
# Other use cases with set()
even = set(range(0, 40, 2))
print(even)
squaresTuple = (4, 6, 9, 26, 25)
squares = set(squaresTuple)
print(squares)

print("=" * 40)

wildAnimals = set(["lion", "tiger", "lion", "panther", "elephant", "hare"])
print(wildAnimals)

for animal in wildAnimals:
    print(animal)

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Adding to sets
# --------------------------------------------------------------------------------------------------------------------
print("Adding horse to both sets")
farmAnimals.add("horse")
wildAnimals.add("horse")
print(farmAnimals)
print(wildAnimals)

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Removing from sets
# --------------------------------------------------------------------------------------------------------------------
print("Removing horse from both sets")
farmAnimals.discard("horse")
wildAnimals.discard("horse")
wildAnimals.discard("funky monkey")  # No error, not in set so does nothing
# wildAnimals.remove("funk monkey")  # WILL THROW ERROR
if "funky monkey" in wildAnimals:  # What you have to do instead
    wildAnimals.remove("funky monkey")
# Another way
try:
    wildAnimals.remove("funky monkey")
except KeyError:
    print("The item funky monkey is not part of the set")
print(farmAnimals)
print(wildAnimals)

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Unions, contains all members from both sets
# --------------------------------------------------------------------------------------------------------------------
print(len(even))
print(len(squares))

print(even.union(squares))
print(squares.union(even))
print(len(even.union(squares)))

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Intersection, contains members that are within BOTH sets
# --------------------------------------------------------------------------------------------------------------------
print(even.intersection(squares))
print(even & squares)
print(squares.intersection(even))
print(squares & even)

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Subtraction of sets, subtracting set B from set A removes any item in set B from set A
# --------------------------------------------------------------------------------------------------------------------
print(sorted(even))
print(sorted(squares))
print("even minus squares")
print(sorted(even.difference(squares)))
print(sorted(even - squares))
print("squares minus evens")
print(sorted(squares.difference(even)))
print(sorted(squares - even))

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Actual modification
# --------------------------------------------------------------------------------------------------------------------
print(sorted(even))
print(squares)
even.difference_update(squares)
print(sorted(even))

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Symmetric difference means all the members in one set or the other but NOT both, can be thought of as opposite of
# intersection
# --------------------------------------------------------------------------------------------------------------------
even = set(range(0, 40, 2))
print(even)
squaresTuple = (4, 6, 9, 26, 25)
squares = set(squaresTuple)
print(squares)

print("Symmetric difference of even minus squares")
print(sorted(even.symmetric_difference(squares)))

print("Symmetric difference of squares minus even")
print(sorted(squares.symmetric_difference(even)))

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Subset/SuperSet
# Subset means that all the members of B exist in A, then B is a subset of A
# Superset means that if all the members of B exist in A, then A is a superset of B
# --------------------------------------------------------------------------------------------------------------------
squaresTuple = (4, 6, 16)
squares = set(squaresTuple)

if squares.issubset(even):
    print("squares is a subset of even")

if even.issuperset(squares):
    print("even is a superset of squares")

print("=" * 40)

# --------------------------------------------------------------------------------------------------------------------
# Frozen set
# Immutable set which means we can use it as a dictionary key
# Can use a frozen set as a member of another set
# --------------------------------------------------------------------------------------------------------------------
even = frozenset(range(0, 100, 2))
print(even)
# even.add(3)  # will error
