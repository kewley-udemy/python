welcome = "Welcome to my Nightmare", "Alice Cooper", 1975
bad = "Bad Company" "Bad Company", 1974
budgie = "Nightflight", "Budgie", 1981
imelda = "More Mayhem", "Emilda May", 2011, (
    (1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")
)
print(imelda)
print('='*50)

title, artist, year, tracks = imelda
print(title)
print(artist)
print(year)
print(tracks)

print('='*50)

# Bad because now we need to know the data behind this when we unpack
# If we don't unpack ALL the tuple variables we will error
anotherMelda = "More Mayhem", "Emilda May", 2011, (1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")
title, artist, year, track1, track2, track3, track4 = anotherMelda
print(title)
print(artist)
print(year)
print(track1)
print(track2)
print(track3)
print(track4)

# First object really represents the correct object structure since an album contains "tracks" rather than
# an album has tracks.
