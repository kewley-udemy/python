# Don't necessarily need parantheses
t = "a", "b", "c"
print(t)

# Case where you would need tuples
print("a", "b", "c")
print(("a", "b", "c"))

welcome = "Welcome to my Nightmare", "Alice Cooper", 1975
bad = "Bad Company" "Bad Company", 1974
budgie = "Nightflight", "Budgie", 1981
imelda = "More Mayhem", "Emilda May", 2011
metallica = "Ride the Lightning", "Metallica", 1984

print(metallica)
print(metallica[0])
print(metallica[1])
print(metallica[2])

# Will throw errors
# TUPLES ARE IMMUTABLE
# metallica[0] = "Master of puppets"

# How you modify tuples, you have to make a new reference
imelda = imelda[0], "Imelda May", imelda[2]  # NOTE right to left when assigning
print(imelda)

# ----------------------------------------------------------------------------------
# Tuples are immutable (same operations as lists), Lists are mutable
# ----------------------------------------------------------------------------------

# Able to modify no error using lists
metallica2 = ["Ride the lightning", "Metallica", 1984]
print(metallica2)
metallica2[0] = "Master of Puppets"
print(metallica2)

# SIDE NOTE WITH VARIABLE ASSIGNMENTS
a, b = 12, 13
print(a, b)
a, b = b, a
print(a, b)

# See below now for using this side note with tuples

# ----------------------------------------------------------------------------------
# "Unpacking" tuples
# ----------------------------------------------------------------------------------
# Extracting values out of a tuple to use different variables
title, artist, year = imelda
print(title)
print(artist)
print(year)

