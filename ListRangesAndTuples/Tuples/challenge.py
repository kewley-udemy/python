# Given the tuple below that represents the Imelda May album "More Mayhem", write
# code to print the album details, followed by a listing of all the tracks in the album.
#
# Indent the tracks by a single tab stop when printing them (remember that you can pass
# more than one item to the print function, separating them with a comma).

imelda = "More Mayhem", "Imelda May", 2011, (
    (1, "Pulling the Rug"), (2, "Psycho"), (3, "Mayhem"), (4, "Kentish Town Waltz")
)

print(imelda)


title, artist, year, tracks = imelda
track1, track2, track3, track4 = tracks
print(title)
print(artist)
print(year)

for song in tracks:
    track, title = song
    print("\tTrack Number: {}, Title: {}".format(track, title))

# So say we have defined our albums as tuples, would we store all the collection of
# albums as tuples or lists?

# A: Better to store in a list because we will most likely add to the collection of albums as time goes on

# Second question: Although is a tuple is immutable, what would be the status of a mutable
# object within a tuple?

# A: A mutable object WITHIN a tuple CAN change

test = "Test", [1, 2, 3]

# Contents have changed!
test[1].append(3)
print(test)
