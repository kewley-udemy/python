# --------------------------------------------------------------------------------
# https://docs.python.org/3/library/stdtypes.html#sequence-types-list-tuple-range
# --------------------------------------------------------------------------------
# ipAddress = input("Please enter an IP address: ")
# print(ipAddress.count("."))

parrotList = ["non pinin'", "no more", "a stiff", "bereft of live"]
parrotList.append("A Norwegian Blue")
for state in parrotList:
    print("This parrot is " + state)

even = [2, 4, 6, 8]
odd = [1, 3, 5, 7, 9]

numbers = even + odd
print(numbers)

# returns a new list with sorted numbers
numbersInOrder = sorted(numbers)
print(numbersInOrder)

# Sort method works on the EXISTING object and does not create a new object
# A lot of other languages create a new collection
numbers.sort()
print(numbers)

# --------------------------------------------------------------------------------
# NOTE: Generally speaking, if a method returns NONE it implies the object itself was modified
# --------------------------------------------------------------------------------

# python will check for same order and same content, not the SAME object
if numbers == numbersInOrder:
    print("The list are equal")
else:
    print("The lists are not equal")
