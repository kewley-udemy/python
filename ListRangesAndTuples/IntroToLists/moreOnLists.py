# Too ways to create empty lists
listOne = []
listTwo = list()

print("List One: {}, List Two: {}".format(listOne, listTwo))
if listOne == listTwo:
    print("The lists are equal")

print(list("The lists are equal"))
print()

# Both variables refer to the EXACT same list, so when we modify one it affects the other
even = [2, 4, 6, 8]
another_even = even
another_even.sort(reverse=True)
print(another_even is even)
print(even)
print()

# How we can avoid it, the list() constructor creates a new list
anotherEven = list(even)
print(anotherEven == even)  # same content
print(anotherEven is even)  # not same reference
print()

evenTwo = [2, 4, 6, 8]
odd = [1, 3, 5, 7, 9]
numbers = [evenTwo, odd]
print(numbers)
print()
for number_set in numbers:
    print(number_set)

    for value in number_set:
        print(value)
print()

menu = []
menu.append(["egg", "spam", "bacon"])
menu.append(["egg", "sausage", "bacon"])
menu.append(["egg", "spam"])
menu.append(["egg", "bacon", "spam"])
menu.append(["egg", "bacon", "sausage", "spam"])
menu.append(["spam", "bacon", "sausage", "spam"])
menu.append(["spam", "egg", "spam", "spam", "bacon", "spam"])
menu.append(["spam", "egg", "sausage", "spam"])

for meal in menu:
    if not "spam" in meal:
        print(meal)
