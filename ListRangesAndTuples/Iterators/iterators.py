string = "1234567890"

# For-loop way
# for char in string:
#     print(char)

myIterator = iter(string)
# print(myIterator)
# print(next(myIterator))
# print(next(myIterator))
# print(next(myIterator))
# print(next(myIterator))
# print(next(myIterator))
# print(next(myIterator))
# print(next(myIterator))
# print(next(myIterator))
# print(next(myIterator))
# print(next(myIterator))

# Will crash since there are no more elements!
# print(next(myIterator))

for char in iter(string):
    print(char)
