decimals = range(0, 100)
myRange = decimals[3:40:3]
print(myRange == range(3, 40, 3))
print('='*50)

# The actual SEQUENCES are the same! We are testing the RESULTS of the range
print(range(0, 5, 2) == range(0, 6, 2))
print(list(range(0, 5, 2)))  # Same as below
print(list(range(0, 6, 2)))
print('='*50)

r = range(0, 10)
print(r)

for i in r[::-2]:
    print(i)
print('='*50)
for i in range(9, 0, -2):
    print(i)
print('='*50)

print(range(0, 100)[::-2] == range(99, 0, -2))

# This WILL NOT print anything since we start at 0 and go back -2 up to 100
for i in range(0, 100, -2):
    print(i)


backString = "egaugnal lufrewop yrev a si nohtyP"
print(backString[::-1])

rTwo = range(0, 10)
for i in rTwo[::-1]:
    print(i)
