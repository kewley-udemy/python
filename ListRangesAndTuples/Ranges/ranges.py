print(range(100))
myList = list(range(10))
print(myList)

even = list(range(0, 10, 2))
odd = list(range(1, 10, 2))

print(even)
print(odd)

# Python ranges are very efficient in memory
# These use the SAME amount of memory
range(0, 10)
range(0, 10000000)
# Creating a list of this range though will take up a lot of memory

# Recap on index
myString = "abcdefghijklmnopqrstuvwxyz"
print(myString.index('e'))
print(myString[4])

# Apply the same to ranges
smallDecimals = range(1, 10000, 2)
print(smallDecimals)
print(smallDecimals.index(985))

# Find divisibility, quite powerful
# sevens = range(7, 10000000, 7)
# x = int(input("Please enter a postive number less than one million: "))
# if x in sevens:
#     print("{} is divisible by seven".format(x))

print('=' * 40)
decimals = range(0, 100)
print(decimals)
myRangeTwo = decimals[3:40:3]

for i in myRangeTwo:
    print(i)

print('=' * 40)

for i in range(3, 40, 3):
    print(i)

print(myRangeTwo == range(3, 40, 3))
