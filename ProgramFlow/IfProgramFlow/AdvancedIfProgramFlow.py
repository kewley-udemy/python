# age = int(input("How old are you? "))

# both must be true with and
# short circuits on the FIRST false case
# if (age >= 16) and (age <= 65):
# if 15 < age < 66:
#     print("Have a good day at work")
#
# print()

# either can be true with or
# short circuits on the FIRST true case
# if (age < 16) or (age > 65):
#     print("Enjoy your free time")
# else:
#     print("Have a good day at work")

# If some_condition evaluates to true, no need to execute the method
# Important to understand for performance reasons
# if (some_condition) or (some_wird_function_that_does_stuff()):
#     # do something


# ----------------------------------------------------------------------
# boolean evaluations with empty data
# ----------------------------------------------------------------------
#TODO: PYTHON DOES NOT HAVE A BOOLEAN DATA TYPE

x = "false"
if x:
    print("x is true")  # will print because x has a value so it evaluates to true


print("""
False: {}
0: {}
0.0: {}
empty list []: {}
empty tuple (): {}
empty string '': {}
empty string "": {}
empty mapping: {}
""".format(False, bool(0), bool(0.0), bool([]), bool(()), bool(''), bool(""), bool({})))

y = input("Please enter some text ")
if y:
    print("You entered '{}'".format(y))
else:
    print("You did not enter anything")

# ----------------------------------------------------------------------
# not keyword
# ----------------------------------------------------------------------
# 'not' Reverses the scenario
print(not False)
print(not True)

age = int(input("How old are you? "))
if not(age < 18):
    print("You are old enough to vote")
    print("Please put an X in the box")
else:
    print("Please come back in {0} years".format(18 - age))

parrot = "Norwegian Blue"
letter = input("Enter a character: ")
if letter not in parrot:
    print("I don't need that letter")
else:
    print("Give me an {}, Bob".format(letter))
