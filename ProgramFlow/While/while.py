# for-loop way
# for i in range(10):
#     print("i is now {}".format(i))

# while-loop way
i = 0
while i < 10:
    print("i is now {}".format(i))
    i += 1

# While loops are useful when you DON'T know when to end a program execution
available_exits = ["east", "north east", "south"]
chosen_exit = ""
while chosen_exit not in available_exits:
    chosen_exit = input("Please choose a direction: ")
    if chosen_exit == "quit":
        print("Game Over")
        break
# only executed if the while loop didn't "break"
else:
    print("Aren't you glad you got out of there!")
