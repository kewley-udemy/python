### NOTE ###
# Notice how the variables are not camelCased, python doesn't care what you use overall
# best to stick with camelCase for myself since I come from more of a Java/JS background

shopping_list = ["milk", "pasta", "eggs", "spam", "bread", "rice"]
for item in shopping_list:
    if item == 'spam':
        print("I am ignoring " + item)
        continue
    print("Buy " + item)

meal = ["egg", "bacon", "spam", "sausages"]
nasty_food_item = ''
for item in meal:
    if item == 'spam':
        nasty_food_item = item
        break
else:  # Notice this is attached to the for-loop and ONLY executes if the full for loop ran
    print("I'll have a plate of that then, please")

if nasty_food_item == 'spam':
    print("Can't I have anything without spam in it")
