# The last value is not specified loop values 1 through 4
for i in range(1, 5):
    print("i is now {}".format(i))

number = "9,223"
for i in range(0, len(number)):
    print(number[i])
print()

number = "9,223"
for i in range(0, len(number)):
    if number[i] in '0123456789':
        print(number[i], end='')
print()

number = "9,223"
cleanedNumber = ''
for i in range(0, len(number)):
    if number[i] in '0123456789':
        cleanedNumber += number[i]

newNumber = int(cleanedNumber)
print("The number is {}".format(newNumber))
