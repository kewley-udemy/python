__author__ = 'dev'

greeting = "Bruce"
_myName = "Tim"
Tim45 = "Good"
Tim_was_57 = "Hello"
Greeting = "There"
print(Tim_was_57 + ' ' + greeting)

age = 24
print(age)

print(greeting + str(age))

# Types
# int/long
# floating/double

a = 12
b = 3
print(a + b)
print(a - b)
print(a * b)
print(a / b)  # returns a float
print(a // b)  # THIS DOES NOT RETURN A FLOAT
print(a % b)

# useful case with a // b
for i in range(1, a // b):
    print(i)
# Operator precedence
# First takes b / 3
# Then takes 4 * 12
print(a + b / 3 - 4 * 12)

print((((a + b) / 3) - 4) * 12)
# Easier on the eyes
c = a + b
d = c / 3
e = d - 4
print(e * 12)

