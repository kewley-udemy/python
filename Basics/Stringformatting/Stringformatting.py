# ------------------------------------------------------------------------------------------------
# https://docs.python.org/2.4/lib/typesseq-strings.html
# https://docs.python.org/3/library/string.html#format-string-syntax
# ------------------------------------------------------------------------------------------------

# STRINGS ARE IMMUTABLE

# String data types (They are sequence data types)
parrot = "Norwegian Blue"
print(parrot)
# Utilizing sequence type
print(parrot[0])  # 0 indexed
print(parrot[3])
print(parrot[-1])  # Last element of the string
print(parrot[0:9])  # Range start at index 0 and go up to NOT including 9
print(parrot[:9])  # Start from the start of the string to 9 exclusive
print(parrot[9:])  # Start at index 9 and go to end
print(parrot[-4:-2])
print(parrot[0:6:2])  # Range start at 0 up to not including 6 in steps of 2

# Useless application but... Extracts information out of a specific format
number = "9,223,372,036,854,775,807"
print(number[1::4])
numbers = "1, 2, 3, 4, 5, 6, 7, 8, 9"
print(numbers[0::3])
days = "Mon, Tue, Wed, Thu, Fri, Sat, Sun"
print(days[::5])

string1 = "he's "
string2 = "probably "
print(string1 + string2)
# No need for plus signs when all strings...
print("he's " "probably " "pining")

# Multiplying strings
print("Hello " * (5 + 4))

# Searching
today = "friday"
print("day" in today)  # day is in friday
print("fri" in today)
print("thur" in today)
print("parrot" in "fjord")

# Formatting a non string data type to a string
age = 24
# Without the str() it will fail at this line
print("My age is " + str(age) + "years")

# Replacement field, much easier than the + str method.
print("My age is {0} years".format(age))

print("There are {0} days in {1}, {2}, {3}, {4}, {5}, {6}, and {7}".format(
    31, "January", "March", "May", "July", "August", "October", "December"
))

# Triple quotes way, CAN REUSE same formats as well and in ANY order
print("""
January: {2}
February: {0}
March: {2}
April: {1}
May: {2}
June: {1}
July: {2}
August: {2}
September: {1}
October: {2}
November: {1}
December: {2}
""".format(28, 30, 31))

# PYTHON 2 related, used all over
print("My age is %d years" % age)
print("My age is %d %s, %d %s" % (age, "years", 6, "months"))

for i in range(1, 12):
    # Two stars means we are raising to the power of
    # The number in front gives the spacing and makes it look formatted nicely
    print("No. %2d squared is %4d and cubed is %4d" % (i, i ** 2, i ** 3))

# Precision of a number, add it after the decimal and before the decimal
print("Pi is approximately %12.50f" % (22 / 7))

# NEW WAY OF FORMATTING
for i in range(1, 12):
    # x:y means argument x with spacing y
    # The < makes things LEFT formatted
    # The > makes things RIGHT formatted
    print("No. {0:2} squared is {1:<4} and cubed is {2:>4}".format(i, i ** 2, i ** 3))

print("Pi is approximately {0:12.50f}".format(22 / 7))

# Can place in ANY order which we couldn't with the % method
print("Test {2} cool {1} omg {0}".format(1, 2, 3))

# don't need the {x} parameter, it will just assume it is in order you specified
for i in range(1, 12):
    print("No. {} squared is {} and cubed is {:>4}".format(i, i ** 2, i ** 3))
