__author__ = 'dev'
# greeting = "Hello"
# name = input("Please enter your name ")
# print(greeting + ' ' + name)

# splitString = "This string has been\nsplit over\nseveral\nlines"
# print(splitString)
#
# tabbedString = "1\t2\t3\t4\t5"
# print(tabbedString)

print('The per shop owner said "No, No, \'e\'s uh, ...he\'s resting"')
print("The per shop owner said \"No, no, e's uh,....he's resting\"")

anotherSplitString =  """This string has been
split over
several lines"""
print(anotherSplitString)

print('''The pet shop said "No no, e's, uh,...he's resting''')
# notice the space from the last quote to the last three, can't have 4 ina row
print("""The pet shop said "No no, e's, uh,...he's resting" """)